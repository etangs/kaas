import sys
import json
import os
f=open(sys.argv[1],'r')
lines=f.readlines()
f.close()
try:
    os.mkdir("output")
except:
    pass
for line in lines[1:]:
    col=line.replace('\n','').replace('\r','').replace(',',';').split(';')
    data={ "code": int(col[0]),
           "title": col[1],
           "artist": col[2],
           "lyric": col[3],
           "language": col[4]
         }
    with open(os.path.join("output",col[0]+".json"),"w") as f:
        json.dump(data,f)
