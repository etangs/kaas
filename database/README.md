# Description of Database Dev

|Parameters | Values|
|---|---|
| Host | chefphan.com |
| Port | 27017 |
| User | admin |
| Password | password123 |
| Database | karaoke |
| Collection | songs |

Document structure: 
```
{ "code" : 30019, "lyric" : "Open your eyes", "artist" : "Jon Bon Jovi", "language" : "EN", "title" : "Always" }
```

# Convert csv file to json files
```
python csv2jsons.py international_songs.csv
```

# Inject into mongo database
```
docker run -it -v $PWD/output:/output mongo bash
ls -1 /output | while read jsonfile; do mongoimport -h chefphan.com -u admin -p password123 --authenticationDatabase admin -d karaoke -c songs --file /output/$jsonfile --type json; done
```